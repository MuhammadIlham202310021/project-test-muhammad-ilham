import "./App.css";
import "./assets/css/main.css";
import HeaderNav from "./components/HeaderNav";
import Slideshow from "./components/Slideshow";
import Card from "./components/Card";
import Form from "./components/Form";
import Footer from "./components/Footer";

function App() {
  return (
    <div className="App">
      <HeaderNav />
      <Slideshow />
      <Card />
      <Form />
      <Footer />
    </div>
  );
}

export default App;
