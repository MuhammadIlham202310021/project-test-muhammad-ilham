import React from "react";
import { Slide } from "react-slideshow-image";
import "react-slideshow-image/dist/styles.css";

const Slideshow = () => {
  return (
    <Slide>
      <div className="each-slide-effect">
        <div className="image1">
          <p>
            THIS IS A PLACE WHERE TECHNOLOGY & CREATIVITY FUSED INTO DIGITAL
            CHEMISTRY
          </p>
        </div>
      </div>
      <div className="each-slide-effect">
        <div className="image2">
          <p>WE DON'T HAVE THE BEST BUT WE HAVE THE GREATEST TEAM</p>
        </div>
      </div>
    </Slide>
  );
};

export default Slideshow;

// import React, { Component } from "react";
// import Slider from "react-slick";
// import "slick-carousel/slick/slick.css";
// import "slick-carousel/slick/slick-theme.css";

// class Slideshow extends Component {
//   render() {
//     const settings = {
//       dots: true,
//       // autoplay: true,
//       infinite: true,
//       slidesToShow: 1,
//       slidesToScroll: 1,
//        indicators: true,
//       arrows: true
//     };

//     return (
//       <div className="slideshow">
//         <Slider {...settings}>
//           <div>
//             <img
//               src={require("../assets/image/bg.jpg")}
//               alt="Credit to Joshua Earle on Unsplash"
//             />
//           </div>
//           <div>
//             <img
//               src={require("../assets/image/about-bg.jpg")}
//               alt="Credit to Alisa Anton on Unsplash"
//             />
//           </div>
//           {/* <div><img src={require('./images/004.PNG')} alt="Credit to Pierre Châtel-Innocenti on Unsplash"/></div>
//         <div><img src={require('./images/005.PNG')} alt="Credit to Richard Nolan on Unsplash"/></div>
//         <div><img src={require('./images/006.PNG')} alt="Credit to Cristina Gottardi on Unsplash"/></div> */}
//         </Slider>
//       </div>
//     );
//   }
// }

// export default Slideshow;
