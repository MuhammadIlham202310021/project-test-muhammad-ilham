import React, { useState } from "react";

const Form = () => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState("");
  const [showWarning, setShowWarning] = useState(false);
  const [showValidationEmail, setShowValidationEmail] = useState(false);

  const handleSubmit = (e) => {
    e.preventDefault();

    if (!name || !email || !message) {
      setShowWarning(true);
      return;
    }

    if (!validateEmail(email)) {
      setShowValidationEmail(true);
      return;
    }

    setShowWarning(false);
    console.log("Data yang dikirim:", name, email, message);
  };

  const validateEmail = (email) => {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(email);
  };

  return (
    <form className="contact-form" onSubmit={handleSubmit}>
      <h1>Contact Us</h1>
      <div className="form-group">
        <label htmlFor="name">Name</label>
        <input
          type="text"
          id="name"
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
        {showWarning && <p className="warning">This field is required</p>}
      </div>
      <div className="form-group">
        <label htmlFor="email">Email</label>
        <input
          type="text"
          id="email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        {showWarning && <p className="warning">Invalid Email Address</p>}
        {showValidationEmail && (
          <p className="warning">Invalid Email Address</p>
        )}
      </div>
      <div className="form-group">
        <label htmlFor="message">Message</label>
        <textarea
          id="message"
          value={message}
          onChange={(e) => setMessage(e.target.value)}
        ></textarea>
        {showWarning && <p className="warning">This field is required</p>}
      </div>
      <button>SUBMIT</button>
    </form>
  );
};

export default Form;
