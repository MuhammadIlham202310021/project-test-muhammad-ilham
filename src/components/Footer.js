import React from "react";

const Footer = () => {
  return (
    <div className="footer">
      <div className="copyright">
        <p>Copyright &copy; 2016. PT Company</p>
      </div>
      <div className="footer-img">
        <img src={require("../assets/image/facebook-official.png")} />
        <img src={require("../assets/image/twitter.png")} />
      </div>
    </div>
  );
};

export default Footer;
