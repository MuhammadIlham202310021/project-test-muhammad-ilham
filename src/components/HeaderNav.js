import React from "react";

const HeaderNav = () => {
  return (
    <header>
      <nav>
        <div className="container">
          <p className="company">Company</p>
          <div className="main-nav">
            <ul>
              <li class="dropdown">
                <a href="javascript:void(0)" class="dropbtn">
                  ABOUT
                </a>
                <div class="sub-menu">
                  <a href="#">HISTORY</a>
                  <a href="#">VISION MISSION</a>
                </div>
              </li>
              <li>
                <a href="/layanan">OUR WORK</a>
              </li>
              <li>
                <a href="/kontak">OUR TEAM</a>
              </li>
              <li>
                <a href="/">CONTACT</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </header>
  );
};

export default HeaderNav;
