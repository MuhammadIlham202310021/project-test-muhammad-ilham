import React from "react";

const Card = () => {
  return (
    <div className="card">
      <div className="judul-card">
        <h1>OUR VALUES</h1>
      </div>
      <div class="row">
        <div class="column">
          <div class="box-1 arrow-right">
            <img src={require("../assets/image/lightbulb-o.png")} />
            <h2>INNOVATIVE</h2>
            <p>
              Lorem ipsum dolor sit amet, consectetur adilpisicing elit. Maxime
              execirtationem dolorem deserunt, unde, eaque ipsa
            </p>
          </div>
        </div>
        <div class="column">
          <div class="box-2 arrow-right">
            <img src={require("../assets/image/bank.png")} />
            <h2>LOYALTY</h2>
            <p>
              Lorem ipsum dolor sit amet, consectetur adilpisicing elit. Maxime
              execirtationem dolorem deserunt, unde, eaque ipsa
            </p>
          </div>
        </div>
        <div class="column">
          <div class="box-3 arrow-right">
            <img src={require("../assets/image/balance-scale.png")} />
            <h2>RESPECT</h2>
            <p>
              Lorem ipsum dolor sit amet, consectetur adilpisicing elit. Maxime
              execirtationem dolorem deserunt, unde, eaque ipsa
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Card;
